#include <iostream>
#include <iterator.h>

void Print(Iterator *iterator) {
    while (iterator->HasNext()) {
        std::cout << iterator->Next() << " ";
    }
    std::cout << std::endl;
}

int main() {
    Empty empty;
    Print(&empty);
    
    Empty* empty_ptr = new Empty();
    Iterator* iterator = empty_ptr;

    Iterator *counter = new Counter(2, 5);
    Print(counter);
    
    delete counter;
    delete iterator;

    std::vector<int> data{1, 2, 3, 5};
    Iterator *vector_iterator = new VectorIterator(data);
    Print(vector_iterator);
    delete vector_iterator;

    {
        Iterator *first = new Counter(1, 5);
        std::vector<int> data{-3, 8, -1};
        Iterator *second = new VectorIterator(data);
        Iterator *concat = new ConcatIterator(first, second);
        Print(concat);
        delete first;
        delete second;
        delete concat;
    }
    return 0;
}
