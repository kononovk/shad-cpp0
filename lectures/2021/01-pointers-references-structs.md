## Лекция 2: Указатели, ссылки, структуры

---

# Резюме (того, что показывал в коде)
  1. `T ar[N]` &mdash; массив (непрерывная последовательность объектов типа `T`)
  1. `T* p` &mdash; указатель на `T`
  1. Операции: разыменование `*p` и взятие адреса `p = &x`
  1. Арифметика: `p + 3`, `p2 - p1`
  1. Ссылка: `int& r = x` (синтаксический сахар для указателя)
  1. Константность (`int* const` VS `const int*`)
  1. Время жизни объектов

---

# Структуры

```c++
struct LogRecord {
    std::string url;
    std::string cookie;
    int64_t response_size;
};
```

```c++
void WriteLogRecord(const LogRecord& r);
```

```c++
LogRecord record;
record.url = "http://yandex.ru";
record.cookie = "yuid=adsfadfqwer134t13u1t98u45t2t5";

WriteLogRecord(record);
```

---

# Структуры

```c++
struct LinkedListNode {
    LinkedListNode* next;
    int value;
};

int TakeNth(LinkedListNode* head, int n) {
    while (n > 0) {
        head = head->next; // Эквивалентно (*head).next.
        --n;
    }
    return head->value;
}
```

---

# Перечисления

```c++
enum class Color {
    Red,
    Green,
    Yellow,
};
```

```c++
Color color1 = Color::Green;
auto color2 = Color::Red;
```

```c++
if (color == Color::Red) {
    std::cout << "Stop!\n";
} else if (color == Color::Yellow) {
    std::cout << "Ready! Set!\n";
} else if (color == Color::Green) {
    std::cout << "Go!\n";
} else {
    std::cout << "Something strange...\n";
}
```

---

# Что напечатает программа?

```c++
void RemoveSpaces(std::string text) {
    int i = 0;
    while (i < std::ssize(text)) {
        if (text[i] == ' ') {
            text.erase(i, 1);
        } else {
            ++i;
        }
    }
}
```

```c++
std::string text = "aba ca ba";
RemoveSpaces(text);
std::cout << text << std::endl;
```

---

# Передача параметров

```c++

void Clear(std::string text) { // text - это копия
    text.clear(); // можем делать со строкой что угодно
}

std::string s = "abacaba";

Clear(s); // в функцию передаётся копия переменной

// s не изменилась

```

---

# Как правильно?

```c++
void Clear(std::string* s_ptr) {
    (*s_ptr).clear(); // или s_ptr->clear();
}

void Clear(std::string& s) { // так правильно, но не по кодстайлу.
    s.clear();
}
```

---

# Передача параметров по указателю

* Если мы хотим, чтобы функция меняла объект который в неё передали &mdash; передаём его по указателю

Примеры:

```c++
void ReadInput(std::vector<int>* input); // выходной (out-) параметр.
```

```c++
void Sort(std::vector<int>* numbers); // in-out параметр.
```

```c++
void FindUniqueElements(
    std::vector<int> all_elements,
    std::vector<int>* unique); // out-параметр.
```

```c++
std::vector<int> all;
ReadInput(&all);
std::vector<int> unique;
FindUniqueElements(all, &unique);
```

---

# Как написать `IsSorted()`

```c++
bool IsSorted(std::vector<int> v);
```

Что если вектор занимает гигабайт?

```c++
bool IsSorted(std::vector<int>* v);
```

Но функция не меняет вектор внутри.

```c++
bool IsSorted(const std::vector<int>& v);
```
```c++
std::vector<int> v;
ReadInput(&v);
if (IsSorted(v)) {
    std::cout << "input is sorted" << std::endl;
}
```

---

# Передача параметров по ссылке на константу
 * Если функция принимает __большой__ объект и не меняет его внутри &mdash; передаём этот объект по константной ссылке
 * Компилятор не даст вам изменить объект внутри функции

```c++
void WriteOutput(const std::vector<int>& answer) {
    for (size_t i = 0; i < answer.size(); ++i) {
        std::cout << answer[i] << std::endl;
    }

    answer[0] = 0; // ошибка компиляции.
}
```

```c++
void FindUniqueElements(
    const std::vector<int>& all_elements,
    std::vector<int>* unique);
```

---

# Неконстантная ссылка?

* Нельзя использовать в аргументах функции, потому что в точке вызова нельзя понять, меняет функция аргумент или нет

```c++
std::string pretious;
auto x = Frobricate(pretious); // что функция делает с pretious?
```

* Используется в STL (стандартной библиотеке)

```c++
int a = 1, b = 2;
std::swap(a, b);
// b == 1, a == 2
```
```c++
void Reverse(std::string* s) {
    for (size_t i = 0; i < (*s).size() / 2; ++i) {
        std::swap((*s)[i], (*s)[(*s).size() - i - 1]);
    }
}
```

---

# Return value optimization

```c++
std::vector<std::vector<int>> MakeZeroMatrix(size_t n) {
    std::vector<std::vector<int>> matrix;
    matrix.resize(n);
    for (size_t i = 0; i < matrix.size(); ++i) {
        matrix[i].resize(n);
    }
    return matrix;
}

std::vector<std::vector<int>> zero = MakeZeroMatrix(128);
```

* Компилятор оптимизирует копирование возвращаемого значения

---

# Передача параметров и возврат значений: резюме

 * Параметр простого типа (`int`, `char`):
   * Передаём по значению: `void f(int x);`
 * Тяжёлый объект (`vector`, `string`):
   * Передаем по константной ссылке:
     `void f(const std::vector<int>& x);`
   * Передаём по указателю, если надо изменить:
     `void f(std::string* s);`
 * Возвращать тяжёлый объект нужно по значению (компилятор применит return value optimization):
     `std::vector<int> ReadInput();`

---

# Вопросы?