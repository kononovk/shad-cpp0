# Управление ресурсами

1. rvalue-ссылки и move-семантика
2. Управление ресурсами и RAII
3. Умные указатели


---

# rvalue-ссылки

 - У выражения есть тип (бессылочный) и value category
 - Основные value category: lvalue и rvalue
 - rvalue-ссылка: `int&& x = 12;`
 - Можно перегружать функции по виду ссылки: `void f(int&)` vs `void f(int&&)`
 - Основное назначение — move-семантика

---

# move-семантика

 - Контейнеры (и не только) обычно являются эксклюзивными владельцами своего содержимого
 - Конструктор копирования и оператор присваивания контейнеров копируют содержимое
 - Для некоторых типов семантика копирования вообще не определена

---

# move-семантика

 - Перегружаем конструктор копирования и оператор присваивания: `T(T&&)`, `T& operator=(T&&)`
 - Забираем владение ресурсом у переданного объекта
 - Не забываем оставлять донора в пустом, но валидном состоянии
 - Указать на `T&&`-перегрузку можно с помощью `std::move`
 - `std::move` нужен всегда, когда вы хотите забрать владение у lvalue

---

# Управление ресурсами

У нас есть `new/delete`. В чем проблема?

```c++
void f(size_t size) {
    int* data = new int[size];
    ...
    delete[] data;
}
```

---

# Можно забыть сделать delete

У функции не всегда один `return`.

```c++
void f(size_t size) {
    int* data = new int[size];
    ...

    if (condition1) {
        ...
        delete[] data;
        return;
    }

    if (condition2) {
        ...
        delete[] data;
        return;
    }
    delete[] data;
}
```

---

# Решение из мира C

```c++
void f(size_t size) {
    int *data = malloc(size * sizeof(int));
    ...
    if (condition) {
        ...
        goto cleanup;
    }

    if (condition2) {
        ...
        goto cleanup;
    }

    ...

cleanup:
    free(data);
}
```

---

# Обработка ошибок

```c++
void f(size_t size) {
    int* data = new int[size];

    g(); // бросает исключение, память утекла

    delete[] data;
}
```

---

# Проблема не только с памятью

```c++
void f(const char* filename) {
    FILE* fout = fopen(filename, "w");

    ...

    fclose(fout);
}
```

---

# Проблема не только с памятью

```c++
class SomeClass {
public:
    void Action() {
        mutex_.lock();

        ... // произошел досрочный выход

        mutex_.unlock();
    }

private:
    std::mutex mutex_;
};
```

---

# В общем виде

* Получаем ресурс
* Функция с ним работает
* Нужно как-то гарантировать возвращение ресурса вне зависимости от того, что происходит в функции
* Есть ли в языке такой механизм?

---

# Деструктор!

```c++
class IFStream {
public:
    IFStream(const char* filename) {
        fin_ = fopen(filename, "r");
    }

    ~IFStream() {
        fclose(fin_);
    }

private:
    FILE* fin_;
};
```

---

# RAII

 - Эта идиома называется RAII (Resource Acquisition Is Initialization)
 - В конструкторе получаем ресурс, деструктор его освобождает
 - В какой-то степени `std::vector` это тоже RAII-обертка
 - RAII вездесуща в STL: `std::ifstream`, `std::lock_guard` и много других.

```c++
void f(const char* filename) {
    IFStream fin(filename);
    // работаем с fin
}
```

---

# RAII для памяти

Проблемы с сырыми указателями?

```c++
File* OpenUrl(std::string_view url) {
    return new File(url);
}

void f(std::string_view url) {
    auto* document = OpenUrl(url);
    ...
    delete document;
}
```

---

# Первый шаг

```c++
class SmartPtr {
public:
    SmartPtr(File* pointer) {
        pointer_ = pointer;
    }

    ~SmartPtr() {
        delete pointer_;
    }

    File& operator*() {
        return *pointer_;
    }

    File* operator->() {
        return pointer_;
    }

private:
    File* pointer_;
};
```

---

# Использование

```c++

SmartPtr OpenUrl(std::string_view url) {
    return SmartPtr(new File(url));
}

void f(std::string_view url) {
    SmartPtr document = OpenUrl(url);
    ...
}
```

---

# Владение ресурсом

```c++
SmartPtr a = SmartPtr(new File(...));
SmartPtr b = a; // как это должно работать?
std::vector<SmartPtr> files;
files.push_back(a);
```

Нам нужно определиться с **семантикой владения**.

---

# Эксклюзивное владение

 - Пусть есть ресурс `R`, и мы положили его в `SmartPtr p`
 - Тогда `R` доступен **только** через `p` (другими словами, `p` **эксклюзивно владеет** ресурсом `R`)
 - Это означает, что `SmartPtr p1 = p` и `p1 = p` запрещены

```c++
class SmartPtr {
    SmartPtr(const SmartPtr&) = delete;
    SmartPtr& operator=(const SmartPtr&) = delete;
};
```

---

# Передача владения

 - Как же тогда возвращать `SmartPtr` из функций?
 - Реализуем **передачу владения**
 - Для этого в C++ есть move-семантика

```c++
class SmartPtr {
    SmartPtr(SmartPtr&& other) : pointer_(other.pointer_) {
        other.pointer_ = nullptr;
    }

    SmartPtr& operator=(SmartPtr&& other) {
        if (this == &other) {
            return *this;
        }
        delete pointer_;
        pointer_ = other.pointer_;
        other.pointer_ = nullptr;
        return *this;
    }
};
```

---

# Эксклюзивное владение

```c++

SmartPtr OpenUrl(std::string_view url) {
    return SmartPtr(new File(url));
}

void f(const std::string& url) {
    // передаем владение ресурсом (new File)
    // из функции OpenUrl в document
    SmartPtr document = OpenUrl(url);
    ...

    SmartPtr b = document; // не скомпилируется

    SmartPtr b = std::move(document); // а вот это ок
    // теперь b владеет ресурсом и отвечает за  его освобождение
}
```

---

# std::unique_ptr

Ровно эта семантика реализована в `std::unique_ptr`

```c++
#include <memory>

std::unique_ptr<File> OpenUrl(std::string_view url) {
    return std::make_unique<File>(url);
}
```

---

# Использование std::unique_ptr

 - `unique_ptr` можно вернуть из функции
 - Можно хранить в векторе или другом контейнере.
 - Как передавать в функции?

```c++

void PassOwnership(std::unique_ptr<File> ptr) { ... }

void Borrow(File *ptr) { ... }

void g() {
    std::unique_ptr<File> ptr = OpenUrl(...);
    PassOwnership(ptr); // не скомпилируется
    Borrow(ptr.get()); // не передаем владение
    PassOwnership(std::move(ptr)); // передаём владение в функцию
}
```

---

# Разделяемое владение

Другая семантика

```c++
void f() {
    AnotherSmartPtr a = AnotherSmartPtr(new DBConnection(...));
    AnotherSmartPtr b = a;
    ...
    // нужно закрыть соединение
}
```

---

# std::shared_ptr

 - Реализует разделяемое (aka совместное) владение
 - Пусть есть ресурс `R` и shared_ptr-ы `p1`, `p2`... на него
 - `R` будет освобожден только тогда, когда удалится последний `shared_ptr` на него

```c++
void g(std::shared_ptr<int> ptr) {
    ...
}

void f() {
    auto a = std::make_shared<int>(5);
    {
        auto b = a;
        g(b);
    }
}
```
---

# std::weak_ptr


```c++

class StateLogger {
public:
    StateLogger(std::shared_ptr<Service> service)
        : service_(service)
    { }

    void Log() {
        if (auto strong = service_.lock()) {
            // strong имеет тип std::shared_ptr<Service>
            std::cerr <<  "Service " << strong->Name()
                << " is " << strong->State() << std::endl;
        }
    }

private:
    std::weak_ptr<Service> service_;
};

```

---

# Владение

  - Эксклюзивное: `std::unique_ptr<T>`, все контейнеры в STL, `std::fstream`
  - Разделяемое: `std::shared_ptr<T>`
  - Отсутствие владения: `T*`, `std::weak_ptr<T>`

---

# Итого

 - RAII — стандарт управления ресурсами в C++
 - Используется в STL для памяти, файлов, потоков, примитивов синхронизации
 - Для работы с памятью:
   * Вместо `new[]/delete[]` используем `std::vector`/другие контейнеры
   * Вместо `new/delete`:
     - `std::make_unique`
     - `std::shared_ptr`
     - `std::weak_ptr`
